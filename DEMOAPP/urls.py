from django.urls import path
from . import views

urlpatterns = [
    # path('', views.launch, name='home-page'),
    path('', views.launcher, name='web-chat'),
    # path('execwebchat/', views.execwebchat, name='execute web-chat'),

    path('chatbot/', views.chatbot, name='execute chat bot'),
    path('startchat/', views.startchat, name='execute web-chat'),
    path('printQ/', views.printQ, name='print questiom'),
    path('selectQ/', views.selectQ, name='select question'),
]
