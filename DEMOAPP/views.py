from django.shortcuts import render
from django .http import HttpResponse
from subprocess import run, PIPE
import sys
import os
from django import forms


questions = """
1: What is Tinkering
2: Who can apply for setting up an ATL?
3: When to apply?
4: Where to apply to ATL establishment?
5: While trying to sign up as a new user for ATL lab setup, a pop-up displayed “email id and UDISE code already exist”, How to proceed?
6: Is uploading the Utilization Certificate mandatory?
7: Should UC for the capital expense (Rs. 10,00,000) and operational expense (Rs. 2,00,000) be created separately?
8: Should UC be created using GFR 12A
9: Should the closing balance on PFMS and bank account match?
10: What are the documents that have to be uploaded and sent to AIM?
11: Unable to login at ATL Compliance portal, how to proceed?
12: Is it mandatory that the Agency name and School’s name on the bank
13: Can documents be uploaded on the ATL Compliance portal without PFMS code?
14: Whether the GeM portal is only for Government schools or Private schools or both Government and
Private Schools?
15: How can I log in as a secondary user in GeM?
16: Is it mandatory to buy all equipment via GeM only? OR If the school wants to purchase a few items, not the complete package, through GeM
17: The required equipment is not available in the GeM list. How to proceed?
18: How to register in PFMS?
19: I have received an email regarding PFMS, but when I click on the link to fill
20: How to retrieve PFMS account if the principal has left and all details were sent on his/her email id and number?
21: How can we add a new account number in the PFMS Account of the school?
22: The bank status f our new bank account is OK, however, the overall status is shown as Pending PD approval, How to proceed?
23: Why should I fill the MyATL dashboard?
24: What is my username and password for the MyATL dashboard?
25: Unable to access the MyATL dashboard?
26: School has lost the MyATL dashboard password. How to proceed?
27: Do we have to fill in all the details on the dashboard at a go?
28:  Are the following modifications allowed in the Atal Tinkering Lab - Installation of Air Conditioning; Soundproofing; Other cosmetic changes? Can funds from the grant be utilized from the same?
29: Can the schools use the ATL grant to construct the entire lab?
30: Can the ATL fund be used for Industrial visits, which is useful for the school students?
31: Is It Necessary to have WI-FI/Broadband connection for the ATL Lab?
32: Will the vendors help in setting up the lab?
33: Forgot the password to access the GeM portal, how to reset it?
34: The school registered for GeM initially through the google form and made a typo while entering the
mobile number because of which we are not getting the OTP.
35: What is my ATL code?
36: What is the link to the query portal?
37: How to prepare the Utilization Certificate?
38: How to correct the school name, if it is displayed wrong in the ATL list?
39: Quit
"""

def launcher(request):
    return render(request, 'DEMOAPP/webchat.html')

def startchat(request):
    print("Anant")
    return {'data': "Hi, Enter your name", 'state': "printq"}


def printQ(request):
    print(request)
    name = request.POST.get('input')
    res = f"I am ATL Answerer {name}. How can I help you. These are some options you can choose from"
    print(res)
    print(questions)
    return {'data': res, 'data1': questions, 'state': "selectq"}

def selectQ(request):
    with open("answers.txt", "r", encoding="utf-8") as hfile:
        sp = hfile.read()

    questionMap = {

    }

    lines = sp.split("\n")
    for line in lines:
        if line.__contains__("="):
            parts = line.split("=")
            questionMap[parts[0]] = parts[1]
    choice = int(request.POST.get('input'))

    answer = ""

    if choice <= 38:
        answer = questionMap[str(choice)]
        print(answer)

    # return render(request, "DEMOAPP/webchat.html", {'data': answer, 'state': "selectq"})
    return {'data': answer, 'state': "selectq"}


def chatbot(request):
    userinput = request.POST.get('input')
    state = request.POST.get('state')

    botresponse = ""
    print("### ", userinput)
    print("### ", state)
    if state == "startchat" or state == "":
        botresponse = startchat(request)
    if state == "printq":
        botresponse = printQ(request)
    if state == "selectq":
        botresponse = selectQ(request)

    return render(request, "DEMOAPP/webchat.html", botresponse)
