
# ATL Answerer on a Website

This is our initiative where we increase our chatbot level to make as a website. Now it is a website & here is the URL

http://3.16.255.18:8000/

This project is Licensed under **GNU General Public License**


## Screenshot

![App Screenshot](https://i.imgur.com/6qz6cCL.png)
## Run it

Go to http://3.16.255.18:8000/ & there you can follow the steps to run the chatbot.

  
## 🚀 About Us
This project is developed by **Anant Gupta** (The owner of this GitLab page) & **Atharv Dixit**. 
